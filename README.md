# TexNet
A set of deep learning models that enable us to convert images of handwritten
math expressions into LaTeX (currently implemented just the character detector)

Contributors: Cameron Cruz, Zen Simone, Kofi Adu

## Install dependencies
Using Conda with Python 3.6:
`conda install numpy opencv keras pillow matplotlib scikit-learn pandas`

# char-detector
## Training
We are using the handwritten math symbols dataset by Xai Nano, which is found on Kaggle:
[Handwritten math symbols dataset](https://www.kaggle.com/xainano/handwrittenmathsymbols/data)

To download datasets from Kaggle using the command line, follow these instructions:
[How to download Kaggle dataset from command line?](https://walter.deback.net/deep-learning/how-to-download-kaggle-dataset-from-command-line/)

Make sure to download the dataset to the data/ folder:
```sh
cd char-detector/data
wget -x -c --load-cookies cookie.txt https://www.kaggle.com/xainano/handwrittenmathsymbols/downloads/data.rar
# Extract data.rar
unrar x data.rar
```

If everything has been installed correctly you should be able to go ahead and run:
`python3 generate_data.py`

To begin training for 100 epochs:
`python train.py 100`

To use the GPU on an AWS EC2 Instance:
`THEANO_FLAGS=device=cuda0,floatX=float32 python train.py 100`

## Testing on Real World Examples
To test the full character detection step on an image in testImgs/:
`python predict.py testImgs/fe1.jpg`
