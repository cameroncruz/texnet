from split_data import class_info

f = open("data/HMSD/labels.txt", "w+")

classes = class_info("data/split/train", "list")

for klass in classes:
    f.write(klass + '\n')

f.close()
