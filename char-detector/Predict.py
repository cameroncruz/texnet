import cv2
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image
from operator import itemgetter
import sys
import os, shutil
from PIL import Image, ImageOps
from Segmenter import Segmenter as Seg
import numpy as np
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Activation
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
K.set_image_dim_ordering('tf')
import sys
import os
from PIL import Image, ImageOps
from xml.dom import minidom
import glob
from scipy import misc
import os.path as path
import time
from tempfile import TemporaryFile
from keras.callbacks import EarlyStopping, TensorBoard
from sklearn.preprocessing import LabelBinarizer
import pandas as pd
from sklearn.metrics import precision_recall_curve, average_precision_score
from HWPreprocessor import HWPreprocessor
from split_data import check_and_mkdir
import json

predictImg = sys.argv[1]

processer = HWPreprocessor()
processed = processer.preprocess(predictImg)
cv2.imwrite("testImgs/topredict.jpg", processed)
predictImg = "testImgs/topredict.jpg"

print "GETTING MODEL..."
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

y_train = np.load('tmp/y_train.npy')
encoder = LabelBinarizer()
y_train = encoder.fit_transform(y_train)


# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
loaded_model.summary()
check_and_mkdir('boundcrops')

if str(predictImg) != "CM":
    for the_file in os.listdir('boundcrops'):
        file_path = os.path.join('boundcrops', the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

    segs = Seg()
    segs.segment(predictImg)
    crops = os.listdir('boundcrops')
    crops.sort()
    print "MAKING PREDICTIONS..."
    for i in range(0, len(crops)):
        img = "boundcrops/{}.jpg".format(i)
        imgArr = np.empty((1, 32, 32, 3))
        imgArr[0] = misc.imread(img, mode='RGB')
        result = loaded_model.predict(imgArr)
        OHResult = np.zeros_like(result)
        OHResult[np.arange(len(result)), result.argmax(1)]=1
        result = encoder.inverse_transform(OHResult)
        #print "\n\nResult:\n\n", result, "\n" #uncomment to see each prediction
        segs.segments[i][0] = result[0]
    print "SAVING TO predictions.json..."
    with open("predictions.json", 'wb') as outfile:
        json.dump(segs.segments, outfile)
    print "DONE"
elif predictImg == "CM":
    testImgs = np.load('tmp/x_test.npy')
    testLabels = np.load('tmp/y_test.npy')
    confMat = {}
    print "Building confidence matrix..."
    for i in range(0, len(testImgs)):
        img = testImgs[i]
        imgArr = np.empty((1, 32, 32, 3))
        imgArr[0] = img
        label = testLabels[i]
        result = loaded_model.predict(imgArr)
        OHResult = np.zeros_like(result)
        OHResult[np.arange(len(result)), result.argmax(1)]=1
        result = encoder.inverse_transform(OHResult)
        toUpdate = confMat.get(label, {})
        pred = toUpdate.get(result[0], 0)
        pred += 1
        toUpdate[result[0]] = pred
        confMat[label] = toUpdate
    print "Done"
    df = pd.DataFrame(confMat)
    df.to_csv('Confidence.csv')
