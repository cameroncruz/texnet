import numpy as np
import cv2 as cv
import os
from split_data import create_train_test_split, check_and_mkdir
from augment_data import create_multi_symbol_img

DATASET_PATH = "data/extracted_images"
SPLIT_DATASET_PATH = "data/split"
OUTPUT_DATASET_PATH = "data/HMSD"
IMG_OUTPUT_FOLDER = OUTPUT_DATASET_PATH + os.sep + "JPEGImages"
ANNOTATION_OUTPUT_FOLDER = OUTPUT_DATASET_PATH + os.sep + "Annotations"

# Create split of individual symbol imgs
create_train_test_split(DATASET_PATH, SPLIT_DATASET_PATH)

check_and_mkdir(OUTPUT_DATASET_PATH)
check_and_mkdir(OUTPUT_DATASET_PATH + os.sep + "ImageSets/Main")

num = 0
for phase in ['train', 'test']:
    sample_file = open(OUTPUT_DATASET_PATH + os.sep + "ImageSets/Main/{}.txt".format(phase), "w+")
    check_and_mkdir(IMG_OUTPUT_FOLDER + os.sep + phase)
    check_and_mkdir(ANNOTATION_OUTPUT_FOLDER + os.sep + phase)

    for subdir, dirs, files in os.walk(SPLIT_DATASET_PATH + os.sep + phase):
        exampleCount = 0
        for f in files:
            if exampleCount >= 3000:
                break
            exampleCount += 1
            filePath = subdir + os.sep + f
            folderPath = SPLIT_DATASET_PATH + os.sep + phase
            outputFile = "{count}.jpg".format(count=num)
            outputFolder = IMG_OUTPUT_FOLDER + os.sep + phase
            metadata = create_multi_symbol_img(filePath, folderPath, outputFile, outputFolder)
            annotationFilename = "{0}/{1}.xml".format(ANNOTATION_OUTPUT_FOLDER + os.sep + phase, num)
            af = open(annotationFilename, 'w')
            annotationStr = '''\
            <annotation>
            	<folder>{FOLDERNAME}</folder>
            	<filename>{FILENAME}</filename>
            	<source>
            		<database>Handwritten Math Symbols Dataset</database>
            	</source>
            	<owner>
            		<name>Cameron Cruz, Zen Simone, Kofi Adu</name>
            	</owner>
            	<size>
            		<width>500</width>
            		<height>375</height>
            		<depth>3</depth>
            	</size>
            	@@@
            </annotation>
            '''.format(FOLDERNAME = IMG_OUTPUT_FOLDER, FILENAME= outputFile)

            for obj in metadata['objects']:
                obj_str = '''\
                <object>
            		<name>{LABEL}</name>
            		<bndbox>
            			<xmin>{XMIN}</xmin>
            			<ymin>{YMIN}</ymin>
            			<xmax>{XMAX}</xmax>
            			<ymax>{YMAX}</ymax>
            		</bndbox>
            	</object>
                @@@
                '''.format(LABEL= obj['classText'], XMIN= int(obj['xmin']), XMAX= int(obj['xmax']), YMIN= int(obj['ymin']), YMAX= int(obj['ymax']))
                annotationStr = annotationStr.replace('@@@', obj_str)
            annotationStr = annotationStr.replace('@@@', '')
            annotationStr = os.linesep.join([s for s in annotationStr.splitlines() if s.strip()])
            af.write(annotationStr)
            af.close()
            sample_file.write(os.getcwd() + os.sep + IMG_OUTPUT_FOLDER + os.sep + outputFile + '\n')
            num += 1
    sample_file.close()
