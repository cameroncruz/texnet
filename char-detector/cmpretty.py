import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from pandas_ml import ConfusionMatrix
from keras.models import model_from_json
from sklearn.preprocessing import LabelBinarizer
import os
'''
df = pd.read_csv('Confidence.csv')
df = df.fillna(0)
print df
#plt.figure()
df.plot()
plt.show()
'''
plt.close('all')
#MODEL
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

y_train = np.load('tmp/y_train.npy')
encoder = LabelBinarizer()
y_train = encoder.fit_transform(y_train)

#DATA
testImgs = np.load('tmp/x_test.npy')
testLabels = np.load('tmp/y_test.npy')
confMat = {}
y_test = testLabels.tolist()
y_pred = np.empty(len(y_test), dtype=object)
print "Building confidence matrix..."
if 'predicted.npy' not in os.listdir('tmp/'):
    for i in range(0, len(testImgs)):
        img = testImgs[i]
        imgArr = np.empty((1, 32, 32, 3))
        imgArr[0] = img
        label = testLabels[i]
        result = loaded_model.predict(imgArr)
        OHResult = np.zeros_like(result)
        OHResult[np.arange(len(result)), result.argmax(1)]=1
        result = encoder.inverse_transform(OHResult)
        y_pred[i] = result[0]
    np.save('tmp/predicted', y_pred)
else:
    y_pred = np.load('tmp/predicted.npy')
y_pred = y_pred.tolist()
cm = ConfusionMatrix(y_test, y_pred)
cm.plot(normalized=True)
fig_size =plt.rcParams["figure.figsize"]
fig_size[0] = 7
fig_size[1] = 7
plt.rcParams["figure.figsize"] = fig_size
plt.xticks(rotation=90, fontsize=7)
plt.yticks(fontsize=7)
plt.tight_layout()
plt.savefig('confmat.png')
plt.show()
print "Done"
