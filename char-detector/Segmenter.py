import cv2
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image
from operator import itemgetter
import sys
import os
from PIL import Image, ImageOps

class Segmenter(object):
    def  __init__(self):
        self.path = ""
        self.segments = []

    def anyOverlap(self, a, b):
        #upper left, bottom left, upper right, bottom right
        return (
        (b[1] <= a[1]+a[3] and b[1] >=a[1] and b[0] <= a[0]+a[2] and b[0] >=a[0]) or
        (b[1]+b[3] <= a[1]+a[3] and b[1]+b[3] >=a[1] and b[0] <= a[0]+a[2] and b[0] >=a[0]) or
        (b[1] <= a[1]+a[3] and b[1] >=a[1] and b[0]+b[2] <= a[0]+a[2] and b[0]+b[2] >=a[0]) or
        (b[1]+b[3] <= a[1]+a[3] and b[1]+b[3] >=a[1] and b[0]+b[2] <= a[0]+a[2] and b[0]+b[2] >=a[0])
        )

    def outerBox(self, initialBoxes):
        pos = 0
        while pos < len(initialBoxes):
            while(pos+1 < len(initialBoxes)):
                a = initialBoxes[pos] #box 1
                b = initialBoxes[pos+1] #box 2 (easier to read code with this naming convention. trust)
                if self.anyOverlap(a,b):#check for overlap
                    if b[0]+b[2]> a[0]+a[2]: #update to full width
                        initialBoxes[pos][2] = (initialBoxes[pos+1][0]+initialBoxes[pos+1][2])-initialBoxes[pos][0]
                    if b[1]+b[3]> a[1]+a[3]: #update to full height
                        initialBoxes[pos][3] = (initialBoxes[pos+1][1]+initialBoxes[pos+1][3])-initialBoxes[pos][1]
                    del initialBoxes[pos+1] #delete double box
                    #print initialBoxes, "\n\n"
                else:
                    break
            pos += 1
        return initialBoxes

    def pasteToCanvas(self, path, numPics, crops):
        listToReturn = crops
        for i in range(0, numPics):
            # Create image canvas
            bgcolor = (0x00, 0x00, 0x00)
            canvas = (500, 375)
            back = Image.new('RGB', canvas, bgcolor)

            img = Image.open(os.path.join(path , '{}.jpg'.format(i)))
            tsCanvas = ()
            #print crops[i][1]['x']
            s,t = 0,0
            if crops[i][1]['w'] >= crops[i][1]['h']:
                tsCanvas = (crops[i][1]['w'], crops[i][1]['w'])
                t = int((crops[i][1]['w'])-crops[i][1]['h']/2)
            else:
                tsCanvas = (crops[i][1]['h'], crops[i][1]['h'])
                s = int((crops[i][1]['h']-crops[i][1]['w'])/2)
            toScaleBG = Image.new('RGB', tsCanvas, bgcolor)

            toScaleBG.paste(img, (s,t))
            #toScaleBG.save(os.path.join(path , '{}t.jpg'.format(i)))
            toScaleBG = toScaleBG.resize((32,32))

            #w = 45
            #x = int((back.size[0]-w)/2)
            #y = int((back.size[1]-w)/2)
            #back.paste(toScaleBG, (x, y))
            listToReturn[i][0] = toScaleBG
            toScaleBG.save(os.path.join(path , '{}.jpg'.format(i)))

        return listToReturn


    def segment(self, path):
        self.path = path
        img = cv2.imread(self.path, 0)
        img2 = cv2.imread(self.path,0)
        ret, thresh = cv2.threshold(img, 127, 255, 0)
        _, contours,hierarchy = cv2.findContours(thresh, 1, 2) #get countours

        initialBoxes = []
        for i in range(0, len(contours)):
            cnt = contours[i]
            x,y,w,h = cv2.boundingRect(cnt)
            box2Add = [x,y,w,h]
            initialBoxes.append(box2Add)

        initialBoxes.sort(key=lambda e: e[0])

        finalBoxes = self.outerBox(initialBoxes) #merge boxes
        crops =[]
        dirpath = 'boundcrops'
        for i in range(0, len(finalBoxes)):
            x,y,w,h = initialBoxes[i]
            crop = img[y:y+h, x:x+w]
            crops.append(["", {'x': x, 'y': y, 'w': w, 'h': h}])
            cv2.imwrite(os.path.join(dirpath , '{}.jpg'.format(i)), crop) #just a test for outputting crops

            cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,0),2)

        toClassify = self.pasteToCanvas(dirpath, len(finalBoxes), crops)
        self.segments = toClassify
        cv2.imwrite("t1_1.jpg", img)
        return self.segments



#imgname = sys.argv[1]
