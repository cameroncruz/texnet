import numpy as np
import cv2 as cv


class HWPreprocessor(object):
    '''
    ===== Handwriting Preprocessor =====
    Image binarization class built using OpenCV
    to preprocess real-world images of handwriting.
    '''

    def __init__(self):
        None

    def preprocess(self, img_path):
        '''
        Run full preprocessing step on an image:
        1) Convert to grayscale
        2) Perform background equalization
        3) Denoise
        3) Perform Otsu thresholding
        4) Invert
        5) Erase lines present on ruled paper
        '''
        img = cv.imread(img_path)  # Read as grayscale
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        img = self.equalizeBg(img)  # Background equalization

        img = cv.GaussianBlur(img, (5, 5), 0)  # Denoise using Gaussian filtering

        # Otsu Thresholding
        retval, img = cv.threshold(img, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)

        # Dilate with horizontal kernel
        kernel = np.ones((4, 1), np.uint8)
        img = cv.dilate(img, kernel, iterations=1)

        img = cv.bitwise_not(img)  # Invert

        img = self.eraseLines(img)  # Remove lines from paper

        return img

    def eraseLines(self, img):
        '''
        Find long lines (i.e. for ruled paper)
        and attempt to remove them.
        '''
        # Apply edge detection method on the image
        edges = cv.Canny(img, 75, 150)

        # This returns an array of r and theta values
        lines = cv.HoughLinesP(edges, 1, np.pi/180, 200, maxLineGap=300)

        if type(lines) is np.ndarray:
            for x in range(0, len(lines)):
                for x1, y1, x2, y2 in lines[x]:
                    cv.line(img, (x1, y1), (x2, y2), (0, 0, 0), 1)

        kernel = cv.getStructuringElement(cv.MORPH_RECT, (10, 2))
        img = cv.erode(img, kernel, iterations=1)
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5, 5))
        img = cv.dilate(img, kernel, iterations=1)

        return img

    def equalizeBg(self, img):
        '''
        Perform background equalization on an image.
        '''
        # Find max pixel value
        max_value = np.max(img)
        backgroundRemoved = img.astype(float)

        # Gaussian blur with large kernel
        blur = cv.GaussianBlur(backgroundRemoved, (151, 151), 50)

        # Operations to equalize background
        backgroundRemoved = backgroundRemoved/blur
        img = (backgroundRemoved*max_value/np.max(backgroundRemoved)).astype(np.uint8)

        return img
