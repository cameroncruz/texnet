import cv2 as cv
import numpy as np
import re
import os
import base64
import random
from PIL import Image, ImageOps
from split_data import class_info, check_and_mkdir

# Returns a random file from folder of images


def get_random_img(folder_path):
    return random.choice([x for x in os.listdir(folder_path)])

# Initialize a dictionary for an obj and its bbox


def init_object_metadata(classText, classId):
    return {
        'classText': classText,
        'classId': classId,
        'xmin': None,
        'xmax': None,
        'ymin': None,
        'ymax': None
    }

# Creates a composite image from a random number of
# preprocessed images from our dataset and returns
# metadata to create a tfrecord


def create_multi_symbol_img(file_path, folder_path, output_file, output_folder):
    print('Creating {outputFile} at {outputFolder}'.format(outputFile=output_file, outputFolder=output_folder))
    img = cv.imread(file_path)
    img = cv.resize(img, (32, 32), interpolation=cv.INTER_AREA)
    #composite = np.zeros((375, 500), np.uint8)  # Black canvas
    classes = class_info(folder_path, "list")  # List of classes of symbols
    num_symbols = np.random.randint(3, 7)  # How many symbols to put in the canvas

    symbols = []
    obj_metadata = {
        'height': 375,
        'width': 500,
        'filename': None,
        'encoded_image_data': None,
        'image_format': b'jpeg',
        'objects': []
    }

    regex = '([^\/]+)(?=\/[^\/]+\.jpg)'  # Matches name of folder the file is in
    match = re.search(regex, file_path)
    classText = match.group(0)  # Label is the folder name
    classId = classes.index(classText)  # Index of class in classes
    symbols.append(img)
    obj_metadata['objects'].append(init_object_metadata(classText, classId))
    '''
    for i in range(0, num_symbols):
        label = random.choice(classes)
        #label = random.choice(['7', 'beta', '{'])
        dir = folder_path + '/' + label
        file = get_random_img(dir)
        symbol_img = cv.imread(dir + '/' + file)
        scale_factor = np.random.uniform(0.8, 1.2)
        symbol_img = cv.resize(symbol_img, None, fx=scale_factor, fy=scale_factor)
        symbols.append(symbol_img)
        obj_metadata['objects'].append(init_object_metadata(label, classes.index(label)))
    '''
    #maxSpacing = (500 - (45*len(symbols)))/len(symbols)
    centerX = 250
    centerY = 187

    for i in range(0, len(symbols)):
        img = symbols[i]
        img = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

        xMin = 0 #int(centerX - (img.shape[0]/2))
        xMax = img.shape[0] #int(centerX + (img.shape[0]/2)) + 1
        yMin = 0 #int(centerY - (img.shape[1]/2))
        yMax = img.shape[1] #int(centerY + (img.shape[1]/2)) + 1
        #composite[yMin:yMax, xMin:xMax] = img

        obj_metadata['objects'][i]['xmin'] = xMin #/ 500.0
        obj_metadata['objects'][i]['xmax'] = xMax #/ 500.0
        obj_metadata['objects'][i]['ymin'] = yMin #/ 375.0
        obj_metadata['objects'][i]['ymax'] = yMax #/ 375.0
    check_and_mkdir(output_folder)

    cv.imwrite(output_folder + os.sep + output_file, img)

    #encoding = base64.b64encode(cv.imencode('.jpg', composite)[1])
    #obj_metadata['encoded_image_data'] = encoding
    obj_metadata['filename'] = output_file
    return obj_metadata
