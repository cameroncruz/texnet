import numpy as np
import cv2 as cv

def preprocess_data(img_path):
    '''Using OpenCV'''
    img = cv.imread(img_path)  # Read as grayscale
    imgCopy = img.copy()
    img = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    img = cv.GaussianBlur(img, (5, 5), 0)  # Denoise using Gaussian filtering

    # Thresholding
    #img = cv.adaptiveThreshold(img, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
    retval, img = cv.threshold(img, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)

    img = cv.bitwise_not(img)  # Invert

    return img
