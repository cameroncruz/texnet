import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Activation
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
K.set_image_dim_ordering('tf')
from matplotlib import pyplot as plt
from matplotlib import image
from operator import itemgetter
import sys
import os
from PIL import Image, ImageOps
from xml.dom import minidom
import glob
from scipy import misc
import os.path as path
import time
from tempfile import TemporaryFile
from keras.callbacks import EarlyStopping, TensorBoard
#from sklearn.metrics import accuracy_score, f1_score
from sklearn.preprocessing import LabelBinarizer



def cnn(size, n_layers):
    # INPUTS
    # size     - size of the input images
    # n_layers - number of layers
    # OUTPUTS
    # model    - compiled CNN

    # Define hyperparamters
    MIN_NEURONS = 20
    MAX_NEURONS = 128
    KERNEL = (3, 3)

    # Determine the # of neurons in each convolutional layer
    steps = np.floor(MAX_NEURONS / (n_layers + 1))
    nuerons = np.arange(MIN_NEURONS, MAX_NEURONS, steps)
    nuerons = nuerons.astype(np.int32)

    # Define a model
    model = Sequential()

    # Add convolutional layers
    for i in range(0, n_layers):
        if i == 0:
            shape = (size[0], size[1], size[2])
            print shape
            model.add(Conv2D(nuerons[i], KERNEL, input_shape=shape, activation='relu'))
        else:
            model.add(Conv2D(nuerons[i], KERNEL, activation='relu'))

        #model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

    # Add max pooling layer
    #model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(MAX_NEURONS, activation='relu'))
    model.add(Dense(50, activation='relu'))

    # Add output layer
    model.add(Dense(82, activation='softmax'))

    # Compile the model
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    # Print a summary of the model
    model.summary()

    return model





#STAGE 1: DATA COLLECT
# fix random seed for reproducibility

seed = 7
np.random.seed(seed)
#TRAIN
trainPath = 'data/HMSD_backup/JPEGImages/train'
images = os.listdir(trainPath)
sample = np.asarray(misc.imread(os.path.join(trainPath , '{}.jpg'.format(1))))
print sample.shape
n_images = len(images)
x_train = np.empty((n_images, sample.shape[0], sample.shape[1], 3))
print x_train.shape[0], x_train.shape[1], x_train.shape[2], x_train.shape[3]

annoPathTrain = 'data/HMSD_backup/Annotations/train'
annotations = os.listdir(annoPathTrain)
y_train = np.empty(n_images, dtype=object)
if 'x_train.npy' not in os.listdir('tmp/'):
    for i in range(0, n_images):
        #set images
        x_train[i] = misc.imread(os.path.join(trainPath , '{}.jpg'.format(i)), mode='RGB')
        # set annotations
        mydoc = minidom.parse(os.path.join(annoPathTrain , '{}.xml'.format(i)))
        label = mydoc.getElementsByTagName('name')[1]
        y_train[i] = label.firstChild.nodeValue
    np.save('tmp/x_train', x_train)
    np.save('tmp/y_train', y_train)
else:
    x_train = np.load('tmp/x_train.npy')
    print x_train.shape
    y_train = np.load('tmp/y_train.npy')

#test
testPath = 'data/HMSD_backup/JPEGImages/test'
images2 = os.listdir(testPath)
n_imagestest = len(images2)
x_test = np.empty((n_imagestest, sample.shape[0], sample.shape[1], 3))

annoPathTest = 'data/HMSD_backup/Annotations/test'
annotations = os.listdir(annoPathTest)
y_test = np.empty(n_imagestest, dtype=object)

if 'x_test.npy' not in os.listdir('tmp/'):
    for i in range(n_images, n_images+n_imagestest):
        #set images
        x_test[i-n_images] = misc.imread(os.path.join(testPath , '{}.jpg'.format(i)), mode='RGB')
        # set annotations
        mydoc = minidom.parse(os.path.join(annoPathTest , '{}.xml'.format(i)))
        label = mydoc.getElementsByTagName('name')[1]
        y_test[i-n_images] = label.firstChild.nodeValue
    np.save('tmp/x_test', x_test)
    np.save('tmp/y_test', y_test)
else:
    x_test = np.load('tmp/x_test.npy')
    y_test = np.load('tmp/y_test.npy')

image_size = np.asarray([x_train.shape[1], x_train.shape[2], x_train.shape[3]])
print image_size
encoder = LabelBinarizer()
y_train = encoder.fit_transform(y_train)
y_test = encoder.fit_transform(y_test)
print("DATA LOADED")

#STAGE 2: train

print("Beginning TRAIN")
model = cnn(image_size, 3)
model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=10, batch_size=32, shuffle=True)
scores = model.evaluate(X_test, y_test, verbose=0)
print("Large CNN Error: %.2f%%" % (100-scores[1]*100))

# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")
