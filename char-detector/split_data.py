import os
import cv2
import numpy as np
from preprocess import preprocess_data

# Check if the given file is an image format


def is_image(f):
    return f.endswith(".png") or f.endswith(".jpg")

# Check if dir exists. If not, mkdir.


def check_and_mkdir(in_dir):
    if not os.path.exists(in_dir):
        print("Creating "+in_dir+"...")
        os.makedirs(in_dir)

# Count the direct one-step sub directories (which will represent the class name)


def class_info(in_dir, mode):
    class_lst = []

    for subdir, dirs, files in os.walk(in_dir):
        class_lst = dirs
        break

    if(mode == "len"):
        return (len(class_lst))
    elif(mode == "list"):
        return sorted(class_lst)

# Return whether the current phase is 'train' or 'test'


def return_phase(num, test_num):
    if (num < test_num):
        return "test" + os.sep
    else:
        return "train" + os.sep

# Create a train-test sub-organized directory from the original class directory


def create_train_test_split(in_dir, split_dir):
    print("Saving train-test splitted images into %s" % (split_dir))
    check_and_mkdir(split_dir)
    class_lst = class_info(in_dir, "list")

    for phase in ["train", "test"]:
        # The output directory will be "./split/[:file_dir]/[:phase]/[:class]"
        phase_dir = split_dir + os.sep + phase
        check_and_mkdir(phase_dir)

        for cls in class_lst:
            cls_dir = split_dir + os.sep + phase + os.sep + cls  # Where to read the image from
            check_and_mkdir(cls_dir)

    for subdir, dirs, files in os.walk(in_dir):
        print("Creating examples for {} ...".format(subdir))
        test_num = int(len(files)*0.1)
        cnt = 0
        for f in files:
            file_path = subdir + os.sep + f
            if(is_image(f)):
                img = preprocess_data(file_path)
                cv2.imwrite(split_dir + os.sep + return_phase(cnt, test_num) +
                            subdir.split("/")[-1] + os.sep + f, img)
                cnt += 1

    return split_dir
